#import des librairies
library(randomForest)
require(caTools)

##Load les données##
data <- read.csv('custdata.csv',header=TRUE) 

#affichage des variables
str(data)

#fonction de création de la formule du modèle
createModelFormula <- function(targetVar, xVars, includeIntercept = TRUE){
  if(includeIntercept){
    modelForm <- as.formula(paste(targetVar, "~", paste(xVars, collapse = '+ ')))
  } else {
    modelForm <- as.formula(paste(targetVar, "~", paste(xVars, collapse = '+ '), -1))
  }
  return(modelForm)
}

#fonction de calcul de r_square 
rSquare <- function(model, test){
  prediction <- predict(model, test)
  SSR <- sum((prediction - mean(test$Valeur.fonciere))^2)
  SST <- sum((test$Valeur.fonciere - mean(test$Valeur.fonciere))^2)
  r_square <-SSR/SST
  return(r_square)
}

#classification variables numeriques et pas numeriques
numVars <- names(which(sapply(data, is.numeric)))
facVars <- names(which(sapply(data, is.factor)))

#creation de la formule pour les modeles
modelForm <- createModelFormula(targetVar = "Valeur.fonciere", xVars = c(numVars[-c(2)]),TRUE)

#Selection aléatoire des données
dataSave <- data
data <- dataSave[1:nrow(dataSave),]

#méthode de validation du modele 10-folds cross validation

#Division des données en 10 lots de tailles égales
folds <- cut(seq(1,nrow(data)), breaks=10, labels=FALSE)

r_square_linears <- c(0)
#r_square_rfs <- c()

#Réalisation de la validation 
for(i in 1:10) {
  set.seed(60616)
  #division des données en lots en utilisant la fonction which
  testIndexes <- which(folds==i, arr.ind=TRUE)
  testData <- data[testIndexes, ]
  trainData <- data[-testIndexes, ]
  #modele lineaire
  model_linear <- lm(modelForm,data=trainData)
  r_linear <-rSquare(model_linear, testData)
  #retention du modele avec le meilleur jeu de données d'entraînement
  if (r_linear>=max(r_square_linears)){
    
    best_model <- model_linear
    best_test <- testData
  }
  r_square_linears <- c(r_square_linears,r_linear)
  
  #model random forest
  #model_rf <- randomForest(modelForm,data=trainData)
  #r_rf <-rSquare(model_rf, testData)
  #r_square_rfs <- c(r_square_rfs,r_rf)
  
}
somme_r_linear = sum(r_square_linears)
#somme_r_rf = sum(r_square_rfs)  
rSquare(best_model,best_test)

