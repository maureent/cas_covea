# cas_dusage_covea

Ce repertoire contient deux fichiers:
- data_cleaning.R: contient le script utilisé pour le prétaitement des données et aboutit la création du fichier custdata.csv qui contient les données traitées.
- base_model.R: contient le script utilisé pour la création du modèle et la sélection du dataset d'entraînement à l'aide de la méthode de 10-folds cross validation.